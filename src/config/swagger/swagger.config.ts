import  { DocumentBuilder} from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
    .setTitle('Finnanciary app')
    .build();