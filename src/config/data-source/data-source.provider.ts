import * as mongoose from 'mongoose';
import { DATABASE_PROVIDER_KEY } from '../../commons/constants';

export const databaseProviders = [
    {
        provide: DATABASE_PROVIDER_KEY,
        useFactory: (): Promise<typeof mongoose> =>
            //mongoose.connect('mongodb://127.0.0.1/local'),
            mongoose.connect('mongodb+srv://hugo123:hugo123@cluster1.wwmmk.mongodb.net/?retryWrites=true&w=majority'),
        
    },
];