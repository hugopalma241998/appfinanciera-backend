
import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { CardTypeSchema } from '../schemas/cardType.schema';

export const CardTypeProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('cardtypes', CardTypeSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];