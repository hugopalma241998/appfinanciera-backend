
import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { BankSchema } from 'src/schemas/bank.schema';


export const BankProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('banks', BankSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];