
import { UserSchema } from 'src/schemas/user.schema';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { Connection } from 'mongoose';

export const userProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>
            connection.model('users', UserSchema),
        inject: [DATABASE_PROVIDER_KEY]
    },
];