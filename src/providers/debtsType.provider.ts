import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { DebtypeSchema } from 'src/schemas/debtsType.schema';

export const DebtsTypeProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('debtsType', DebtypeSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];
