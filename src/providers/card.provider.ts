import { CardSchema } from 'src/schemas/card.schema';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { Connection } from 'mongoose';

export const cardProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>
            connection.model('cards', CardSchema),
        inject: [DATABASE_PROVIDER_KEY]
    },
];