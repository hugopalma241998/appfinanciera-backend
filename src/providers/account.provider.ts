import { AccountSchema } from 'src/schemas/Account.schema';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { Connection } from 'mongoose';

export const AccountProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>
            connection.model('Account', AccountSchema),
        inject: [DATABASE_PROVIDER_KEY]
    },
];
