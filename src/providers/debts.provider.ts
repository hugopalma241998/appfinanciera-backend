import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { DebtSchema } from 'src/schemas/debts.schema';

export const DebtProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('debts', DebtSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];
