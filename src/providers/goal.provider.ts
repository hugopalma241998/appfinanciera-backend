import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { DebtSchema } from 'src/schemas/debts.schema';
import { GolaSchema } from 'src/schemas/goal.schema';

export const goalProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('goals', GolaSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];
