
import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { AdviceSchema } from 'src/schemas/advice.schema';

export const AdviceProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('advice', AdviceSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];