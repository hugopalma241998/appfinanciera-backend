import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { AccountTypeSchema } from '../schemas/AccountType.schema';

export const AccountTypeProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('Accounttypes', AccountTypeSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];
