
import { Connection } from 'mongoose';
import { USER_PROVIDER_KEY, DATABASE_PROVIDER_KEY } from '../commons/constants';
import { RolSchema } from '../schemas/rol.schema';

export const rolProviders = [
    {
        provide: USER_PROVIDER_KEY,
        useFactory: (connection: Connection) =>  connection.model('rol', RolSchema),
        inject: [DATABASE_PROVIDER_KEY],
    }
];