import { Module } from '@nestjs/common';
import { RoleModule } from './modules/role.module';
import { UserModule } from './modules/user.module';
import { JwtModule } from '@nestjs/jwt';
import { USER_PROVIDER_KEY, jwtConstants } from './commons/constants';
import { AuthGuard } from './guard/auth.guard';
import { BankModule } from './modules/bank.module';
import { CardTypeModule } from './modules/cardType.module';
import { CardModule } from './modules/card.module';
import { AccountModule } from './modules/account.module';
import { AccountTypeModule } from './modules/accountType.module';
import { AdviceModule } from './modules/advice.module';
import { DebtsModule } from './modules/debts.module';
import { DebtsTypeModule } from './modules/debtsType.module';
import { GoalModule } from './modules/goal.module';

@Module({
  imports: [
    RoleModule,
    UserModule,
    BankModule,
    CardTypeModule,
    CardModule,
    AccountModule,
    AccountTypeModule,
    AdviceModule,
    DebtsTypeModule,
    DebtsModule,
    GoalModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60000000s' },
    }),
  ],

  
})
export class AppModule {}
