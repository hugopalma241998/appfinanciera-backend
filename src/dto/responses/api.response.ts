export class ApiResponse {
    timestamp = new Date().toISOString();
    status = true;
    data: any = {};
    errors = [];
}