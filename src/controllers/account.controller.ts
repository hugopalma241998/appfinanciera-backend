import { Controller, Get, Param, Post, UseGuards, Request, Delete, Put } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { AccountService } from "src/services/Account.service";
@ApiTags('Account')
@Controller('Account')
export class AccountController {
    response: ApiResponse
    
    constructor(
        private AccountService: AccountService,
        ) {
        this.response = new ApiResponse();
    }

    @UseGuards(AuthGuard)
    @Get('/')
    async getAccounts() {

        
        try {
            const users = await this.AccountService.getAccounts();
            this.response.data  = users;
            this.response.status = true;

        } catch (e: unknown) {
            console.log(e);
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }


    
    @UseGuards(AuthGuard)
    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();
        console.log(request.body);
        try {
            const register = await this.AccountService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }


    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.AccountService.getAccount(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Get('/user/:_id/:_idType')
    async findByUser(@Param('_id') _id: string, @Param('_idType') _idType: string ) {
        try {
            const user = await this.AccountService.getAccountsByUser(_id, _idType);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    
    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.AccountService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
        return this.response;

    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.AccountService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
        return this.response;

    }

}
