import { Controller, Delete, Get, Param, Post, Put, Request, UseGuards } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002, E003, E004, E005 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { AuthService } from "src/services/auth.service";
import { UserService } from "src/services/user.service";

@ApiTags('Users')
@Controller('users')
export class UsersController {
    
    response: ApiResponse
    
    constructor(
        private userService: UserService,
        private authService: AuthService
        ) {
        this.response = new ApiResponse();
    }

    @Post('/validateUser')
    async validateUser(@Request() request: any) {
        this.response = new ApiResponse();
        try {
            const register = await this.authService.validateNewUser(request.body.email, request.body.username, request.body.phone);
            console.log(register);
            if(register.length > 0) {
                this.response.status = false;

                let error = '';
                if(register[0].email === request.body.email) 
                {
                    error = E003;
                }
                if(register[0].username === request.body.username) 
                {
                    error = E004;
                }
                if(register[0].phone === request.body.phone) 
                {
                    error = E005;
                }
                this.response.errors.push(error);

            } 
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        
        return this.response;
    }
    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();
       // console.log(request.body);
        try {
            const register = await this.userService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Get('/')
    async getUsers() {
        try {
            const users = await this.userService.getUsers();

            this.response.data  = users;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
    
    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.userService.getUser(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }


    @Get('auth/:_id')
    async findBy(@Param('_id') _id: string ) {
        try {
            const user = await this.userService.validationEmail(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        // creturn this.response;
    }



    @Post('/auth')
    async login(@Request() request: any) {
        this.response = new ApiResponse();
        try {
            const register = await this.authService.signIn(request.body.email, request.body.password);
            this.response.data = register;
            
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        
        return this.response;
    }

    

    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.userService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.userService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }
}