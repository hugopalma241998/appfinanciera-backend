import { Controller, Get, Param, Post, UseGuards, Request, Delete, Put } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { CardService } from "src/services/card.service";
@ApiTags('Card')
@Controller('card')
export class CardController {
    response: ApiResponse
    
    constructor(
        private cardService: CardService,
        ) {
        this.response = new ApiResponse();
    }

    @UseGuards(AuthGuard)
    @Get('/')
    async getCards() {

        
        try {
            const users = await this.cardService.getCards();
            console.log(users);
            this.response.data  = users;
            this.response.status = true;

        } catch (e: unknown) {
            console.log(e);
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }


    
    @UseGuards(AuthGuard)
    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();
        console.log(request.body);
        try {
            const register = await this.cardService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }


    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.cardService.getCard(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Get('/user/:_id/:_idType')
    async findByUser(@Param('_id') _id: string, @Param('_idType') _idType: string ) {
        try {
            const user = await this.cardService.getCardsByUserDebitCredit(_id, _idType);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    
    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.cardService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
        return this.response;

    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.cardService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

}