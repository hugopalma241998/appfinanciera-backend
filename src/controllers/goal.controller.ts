import { Controller, Get, Param, Post, UseGuards, Request, Delete, Put } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { GoalService } from "src/services/goal.service";

@ApiTags('Goals')
@Controller('goals')
export class GoalsController {

    response: ApiResponse
    
    constructor(
        private cardTypeService: GoalService,
        ) {
        this.response = new ApiResponse();
    }


    
    @UseGuards(AuthGuard)
    @Get('/user/:_id')
    async findByUser(@Param('_id') _id: string) {
        try {
            const user = await this.cardTypeService.getAccountsByUser(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();
        console.log(request.body);
        try {
            const register = await this.cardTypeService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            console.log(e);
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }

    
    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.cardTypeService.getOne(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.cardTypeService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }

        return this.response;
    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.cardTypeService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

}