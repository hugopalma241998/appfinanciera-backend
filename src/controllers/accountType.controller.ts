import { Controller, Get, Param, Post, UseGuards, Request, Delete, Put } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { AccountTypeService } from "src/services/AccountType.service";

@ApiTags('AccountType')
@Controller('AccountType')
export class AccountTypeController {

    response: ApiResponse
    
    constructor(
        private AccountTypeService: AccountTypeService,
        ) {
        this.response = new ApiResponse();
    }

    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.AccountTypeService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Get('/')
    async getUsers() {
        try {
            const users = await this.AccountTypeService.get();

            this.response.data  = users;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
    
    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.AccountTypeService.getOne(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.AccountTypeService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.AccountTypeService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

}
