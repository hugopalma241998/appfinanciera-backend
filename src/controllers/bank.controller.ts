import { Controller, Delete, Get, Param, Post, Put, Request, UseGuards } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { E001, E002 } from "src/commons/errors";
import { ApiResponse } from "src/dto/responses/api.response";
import { AuthGuard } from "src/guard/auth.guard";
import { AuthService } from "src/services/auth.service";
import { BankService } from "src/services/bank.service";

@ApiTags('Banks')
@Controller('banks')
export class BankController {

    response: ApiResponse
    
    constructor(
        private bankService: BankService
        ) {
        this.response = new ApiResponse();
    }

    @UseGuards(AuthGuard)

    @Post()
    async create(@Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.bankService.create(request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E002);
        }
        
        return this.response;
    }

    @UseGuards(AuthGuard)
    @Get('/')
    async getUsers() {
        try {
            const users = await this.bankService.getBanks();

            this.response.data  = users;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
    @UseGuards(AuthGuard)
    @Get(':_id')
    async findById(@Param('_id') _id: string ) {
        try {
            const user = await this.bankService.getBank(_id);

            this.response.data  = user;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
    
    @UseGuards(AuthGuard)
    @Put(':_id')
    async update (@Param('_id') _id:string, @Request() request: any) {
        this.response = new ApiResponse();

        try {
            const register = await this.bankService.update(_id, request.body);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }

    @UseGuards(AuthGuard)
    @Delete(':_id')
    async delete(@Param() _id: string, @Request() request: any ) {
        this.response = new ApiResponse();
        try {
            const register = await this.bankService.delete(_id);

            if(register) {
                this.response.data = register;
            } else {
                this.response.status = false;
                this.response.errors.push(E001);
            }
        } catch (e) {
            this.response.status = false;
            this.response.errors.push(E001);
        }
    }
}