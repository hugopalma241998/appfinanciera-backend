import { Controller, Get } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ApiResponse } from "src/dto/responses/api.response";
import { RolService } from "src/services/rol.service";

@ApiTags('Roles')
@Controller('roles')
export class RolesController {
    response: ApiResponse

    constructor(private roleService: RolService) {
        this.response = new ApiResponse();
    }
    
    @Get('/')
    async getRoles() {
        try {
            const roles = await this.roleService.getRoles();

            this.response.data  = roles;
            this.response.status = true;

        } catch (e: unknown) {
            this.response.status = false;
            this.response.errors.push(e);
        }
        return this.response;
    }
}