import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const GolaSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
        income: {type:Number, required:true},
        feeSaving: {type:Number, required:true},
        bills: {type:Number, required:true},    
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },
        status: {type:Boolean, required: true},
    },
    {
        versionKesy: false,
        timestamps: true,
        strict: false
    },
);
