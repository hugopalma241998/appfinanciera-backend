import * as mongoose from 'mongoose';

export const RolSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
    },
    {
        versionKey: false,
        timestamps: true,
    },
);
