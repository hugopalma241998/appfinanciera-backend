import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const AccountSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
        amountAvailable: {type:Number, required:true},
        AccountTypeId: {
            type: Schema.Types.ObjectId,
            ref: 'AccountTypes'
        },
        bankId: {
            type: Schema.Types.ObjectId,
            ref: 'banks'
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },
        status: {type:Boolean, required: true},
    },
    {
        versionKey: false,
        timestamps: true,
    },
);
