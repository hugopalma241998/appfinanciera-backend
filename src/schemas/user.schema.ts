import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
   {
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},

    phone: {type: Number, required: true},
    username: {type: String, required: true},
    status: {type:Boolean, required: true},
    rolId: {
        type: Schema.Types.ObjectId,
        ref: 'roles'
    },
    firstLogin: {type: Boolean, require: true, default: false}
   },
   {
    versionKey: false,
    timestamps: true,
   }
);
