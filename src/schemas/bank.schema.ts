import * as mongoose from 'mongoose';

export const BankSchema = new mongoose.Schema(
    {
        name: {type: String, required: true},
        description: {type: String, required: true},
        status: {type:Boolean, required: true},

    },
    {
     versionKey: false,
     timestamps: true,
    }
);