import * as mongoose from 'mongoose';

export const DebtypeSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
        status: {type:Boolean, required: true},
    },
    {
        versionKey: false,
        timestamps: true,
    },
);
