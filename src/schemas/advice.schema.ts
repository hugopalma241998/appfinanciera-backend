import * as mongoose from 'mongoose';

export const AdviceSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
        url: {type:String, required:true},

        status: {type:Boolean, required: true},
    },
    {
        versionKey: false,
        timestamps: true,
    },
);

