import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const CardSchema = new mongoose.Schema(
    {
        name: {type:String, required:true},
        description: {type:String, required:true},
        limit: {type:Number, required:true},
        amountAvailable: {type:Number, required:true},
        dateOfPayment: {type:Date, required:true},
        dateOfCut: {type:Date, required:true},
        cardTypeId: {
            type: Schema.Types.ObjectId,
            ref: 'cardtypes'
        },
        bankId: {
            type: Schema.Types.ObjectId,
            ref: 'banks'
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },
        status: {type:Boolean, required: true},
    },
    {
        versionKey: false,
        timestamps: true,
    },
);
