import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { DebtsController } from "src/controllers/debt.controller";
import { GoalsController } from "src/controllers/goal.controller";
import { DebtProviders } from "src/providers/debts.provider";
import { goalProviders } from "src/providers/goal.provider";
import { DebtsService } from "src/services/debts.service";
import { GoalService } from "src/services/goal.service";

@Module({
    controllers: [ GoalsController],
    providers: [GoalService, ...goalProviders ],
    imports: [DataSourceModule],
    exports: [GoalService],
})
export class GoalModule {}