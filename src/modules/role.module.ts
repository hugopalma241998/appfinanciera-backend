import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { RolesController } from "src/controllers/roles.controller";
import { rolProviders } from "src/providers/rol.provider";
import { RolService } from "src/services/rol.service";


@Module({
    controllers: [ RolesController],
    providers: [RolService, ...rolProviders ],
    imports: [DataSourceModule],
    exports: [RolService],
})
export class RoleModule {}