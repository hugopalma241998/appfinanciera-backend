import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { UsersController } from "src/controllers/users.controller";
import { userProviders } from "src/providers/user.provider";
import { AuthService } from "src/services/auth.service";
import { UserService } from "src/services/user.service";




@Module({
    controllers: [ UsersController ],
    providers: [UserService, AuthService, ...userProviders ],
    imports: [DataSourceModule],
    exports: [UserService, AuthService],
})
export class UserModule {}