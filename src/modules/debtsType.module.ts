import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { DebtsTypeController } from "src/controllers/debtsType.controller";
import { DebtsTypeProviders } from "src/providers/debtsType.provider";

import { DebtsTypeService } from "src/services/debtsType.service";

@Module({
    controllers: [ DebtsTypeController],
    providers: [DebtsTypeService, ...DebtsTypeProviders ],
    imports: [DataSourceModule],
    exports: [DebtsTypeService],
})
export class DebtsTypeModule {}