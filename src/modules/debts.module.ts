import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { DebtsController } from "src/controllers/debt.controller";
import { DebtProviders } from "src/providers/debts.provider";
import { DebtsService } from "src/services/debts.service";

@Module({
    controllers: [ DebtsController],
    providers: [DebtsService, ...DebtProviders ],
    imports: [DataSourceModule],
    exports: [DebtsService],
})
export class DebtsModule {}