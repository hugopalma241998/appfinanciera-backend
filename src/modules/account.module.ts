import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { AccountController } from "src/controllers/Account.controller";
import { AccountProviders } from "src/providers/Account.provider";
import { AccountService } from "src/services/Account.service";
@Module({
    controllers: [ AccountController],
    providers: [AccountService, ...AccountProviders ],
    imports: [DataSourceModule],
    exports: [AccountService],
})
export class AccountModule {}
