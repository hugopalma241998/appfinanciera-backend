import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { AdviceController } from "src/controllers/advice.controller";
import { AdviceProviders } from "src/providers/advice.provider";
import { AdviceService } from "src/services/advice.service";

@Module({
    controllers: [ AdviceController],
    providers: [AdviceService, ...AdviceProviders ],
    imports: [DataSourceModule],
    exports: [AdviceService],
})
export class AdviceModule {}