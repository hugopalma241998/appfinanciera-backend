

import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { BankController } from "src/controllers/bank.controller";
import { BankProviders } from "src/providers/bank.provider";
import { BankService } from "src/services/bank.service";
@Module({
    controllers: [ BankController],
    providers: [BankService, ...BankProviders ],
    imports: [DataSourceModule],
    exports: [BankService],
})
export class BankModule {}