import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { BankController } from "src/controllers/bank.controller";
import { AccountTypeController } from "src/controllers/AccountType.controller";
import { BankProviders } from "src/providers/bank.provider";
import { AccountTypeProviders } from "src/providers/AccountType.provider";
import { BankService } from "src/services/bank.service";
import { AccountTypeService } from "src/services/AccountType.service";
@Module({
    controllers: [ AccountTypeController],
    providers: [AccountTypeService, ...AccountTypeProviders ],
    imports: [DataSourceModule],
    exports: [AccountTypeService],
})
export class AccountTypeModule {}
