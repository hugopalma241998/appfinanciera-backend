import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { BankController } from "src/controllers/bank.controller";
import { CardTypeController } from "src/controllers/cardsType.controller";
import { BankProviders } from "src/providers/bank.provider";
import { CardTypeProviders } from "src/providers/cardType.provider";
import { BankService } from "src/services/bank.service";
import { CardTypeService } from "src/services/cardType.service";
@Module({
    controllers: [ CardTypeController],
    providers: [CardTypeService, ...CardTypeProviders ],
    imports: [DataSourceModule],
    exports: [CardTypeService],
})
export class CardTypeModule {}