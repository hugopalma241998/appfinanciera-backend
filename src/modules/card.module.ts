import { Module } from "@nestjs/common";
import { DataSourceModule } from "src/config/data-source/data-source.module";
import { CardController } from "src/controllers/card.controller";
import { cardProviders } from "src/providers/card.provider";
import { CardService } from "src/services/card.service";
@Module({
    controllers: [ CardController],
    providers: [CardService, ...cardProviders ],
    imports: [DataSourceModule],
    exports: [CardService],
})
export class CardModule {}