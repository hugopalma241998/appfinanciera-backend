import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from 'src/commons/constants';
import { UserModel } from 'src/models/user.model';


@Injectable()
export class UserService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly userModel: Model<UserModel>,

        
    ) {}

    
    async getUsers(): Promise<any[]> {
        return await this.userModel.find().exec();
    }

    async validationEmail(_id: string): Promise<UserModel> {
       const userModel =  await this.userModel.find({_id: _id, status:true});
       let newValidationUserModel = userModel[0];
       newValidationUserModel.firstLogin = true;
       return await this.userModel.findByIdAndUpdate(_id, newValidationUserModel, {new: true});

    }
    async validationUser(email: string, username: string, phone: string): Promise<any> {
        return await this.userModel.find({ $or: [{email: email}, {username: username}, {phone: phone}] });

    }
    async getUsersActivate(): Promise<any[]> {
        return await this.userModel.find({status: true}).exec();
    }
    async getUser(_id: string): Promise<any[]> {
        return await this.userModel.find({_id: _id, status:true});
    }

    async findLogin(email: string, password: string): Promise<any[]>  {
        return await this.userModel.find({email: email, password: password, firstLogin: true});
    }
    
    async create(userModel: UserModel): Promise<any>{

        return await this.userModel.create(userModel);
    }

    async update(_id: string, userModel: UserModel): Promise<any> {
        return await this.userModel.findByIdAndUpdate(_id, userModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.userModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}