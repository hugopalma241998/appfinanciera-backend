import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";

import { Model, Types } from 'mongoose';
import { DebtTypeModel } from 'src/models/debtsType.model';
import { DebtsModel } from 'src/models/debts.model';
import { GoalModel } from 'src/models/goal.model';



@Injectable()
export class GoalService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly cardTypeModel: Model<GoalModel>,

        
    ) {}


    async getAccountsByUser(_id: string): Promise<any[]> {
        const query = [
            {
                $match: {
                    userId: new Types.ObjectId(_id),
                    status: true
                }
            }, 
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]
        

        return await this.cardTypeModel.aggregate(query);;

    }

    
    async getOne(_id: string): Promise<any[]> {
        return await this.cardTypeModel.find({_id: _id, status:true});
    }

    async create(model: DebtsModel): Promise<any>{
        return await this.cardTypeModel.create(model);
    }

    async update(_id: string, cardTypeModel: DebtsModel): Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, cardTypeModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }


}