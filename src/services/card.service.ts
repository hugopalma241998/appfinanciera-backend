import { Model, Schema } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from 'src/commons/constants';
import { CardModel } from 'src/models/card.model';
import { Types } from 'mongoose';

@Injectable()
export class CardService {

    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly cardModel: Model<CardModel>,
    ) { }




    async getCardsByUserDebitCredit(_id: string, _idType): Promise<any[]> {
        const query = [
            {
                $match: {
                    userId: new Types.ObjectId(_id),
                    cardTypeId: new Types.ObjectId(_idType),
                    status: true
                }
            },
            {
                $lookup:
                {
                    from: 'cardtypes',
                    localField: 'cardTypeId',
                    foreignField: '_id',
                    as: 'cardtype'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]



        return await this.cardModel.aggregate(query);;

    }
    async getCards(): Promise<any[]> {
        const query = [

            {
                $lookup:
                {
                    from: 'cardtypes',
                    localField: 'cardTypeId',
                    foreignField: '_id',
                    as: 'cardtype'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]
        return await this.cardModel.aggregate(query);
    }
    async getCard(_id: string): Promise<any[]> {
        return await this.cardModel.find({ _id: _id, status: true });
    }

    async create(userModel: CardModel): Promise<any> {
        //console.log(userModel);
        return await this.cardModel.create(userModel);
    }

    async update(_id: string, cardModel: CardModel): Promise<any> {
        return await this.cardModel.findByIdAndUpdate(_id, cardModel, { new: true });
    }

    async delete(_id: string): Promise<any> {
        return await this.cardModel.findByIdAndUpdate(_id, { status: false }, { new: true });
    }

}