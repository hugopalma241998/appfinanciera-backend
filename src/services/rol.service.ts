import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";
import { RolModel } from "src/models/rol.model";
import { Model } from 'mongoose';

@Injectable()
export class RolService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly rolModel: Model<RolModel>
    ) {}

    async getRoles(): Promise<any[]> {
    
        return await this.rolModel.find().exec();
    }
}