import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService
  ) {}

  async signIn(email, pass) {
    const user = await this.usersService.findLogin(email, pass);
   
    const payload = { 
      id: user[0]._id,
      username: user[0].username,  
      fullName: user[0].name, 
      email: user[0].email,
      firstLogin: user[0].firstLogin
      };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }


  async validateNewUser(email,username, phone) {
    return await this.usersService.validationUser(email, username, phone);
  }
}