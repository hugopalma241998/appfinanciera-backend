import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from 'src/commons/constants';
import { BankModel } from 'src/models/bank.model';

@Injectable()
export class BankService {
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly bankModel: Model<BankModel>,

        
    ) {}

    async getBanks(): Promise<any[]> {
        return await this.bankModel.find().exec();
    }
    async getBank(_id: string): Promise<any[]> {
        return await this.bankModel.find({_id: _id, status:true});
    }
    async create(userModel: BankModel): Promise<any>{

        return await this.bankModel.create(userModel);
    }

    async update(_id: string, bankModel: BankModel): Promise<any> {
        return await this.bankModel.findByIdAndUpdate(_id, bankModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.bankModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}