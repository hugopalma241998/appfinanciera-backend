import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";
import {  CardTypeModel } from "src/models/cardType.model";
import { Model } from 'mongoose';
import { AdviceModel } from 'src/models/advice.model';



@Injectable()
export class AdviceService {

    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly adviceModel: Model<AdviceModel>

    ) {}

    async get(): Promise<any[]> {
        return await this.adviceModel.find().exec();
    }

    async getOne(_id: string): Promise<any[]> {
        return await this.adviceModel.find({_id: _id, status:true});
    }

    async create(model: AdviceModel): Promise<any>{
        console.log(model);
        return await this.adviceModel.create(model);
    }

    async update(_id: string, adviceModel: AdviceModel): Promise<any> {
        return await this.adviceModel.findByIdAndUpdate(_id, adviceModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.adviceModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }
}
    