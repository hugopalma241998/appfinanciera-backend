import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";
import {  CardTypeModel } from "src/models/cardType.model";
import { Model } from 'mongoose';



@Injectable()
export class CardTypeService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly cardTypeModel: Model<CardTypeModel>,

        
    ) {}

    
    async get(): Promise<any[]> {
        return await this.cardTypeModel.find().exec();
    }

    async getOne(_id: string): Promise<any[]> {
        return await this.cardTypeModel.find({_id: _id, status:true});
    }

    async create(model: CardTypeModel): Promise<any>{
        console.log(model);
        return await this.cardTypeModel.create(model);
    }

    async update(_id: string, cardTypeModel: CardTypeModel): Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, cardTypeModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}