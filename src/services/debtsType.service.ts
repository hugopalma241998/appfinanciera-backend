import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";
import { Model } from 'mongoose';
import { DebtTypeModel } from 'src/models/debtsType.model';



@Injectable()
export class DebtsTypeService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly cardTypeModel: Model<DebtTypeModel>,

        
    ) {}

    
    async get(): Promise<any[]> {
        return await this.cardTypeModel.find().exec();
    }

    async getOne(_id: string): Promise<any[]> {
        return await this.cardTypeModel.find({_id: _id, status:true});
    }

    async create(model: DebtTypeModel): Promise<any>{
        console.log(model);
        return await this.cardTypeModel.create(model);
    }

    async update(_id: string, cardTypeModel: DebtTypeModel): Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, cardTypeModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}