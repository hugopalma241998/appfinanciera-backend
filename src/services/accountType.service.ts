import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";
import {  AccountTypeModel } from "src/models/AccountType.model";
import { Model } from 'mongoose';



@Injectable()
export class AccountTypeService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly AccountTypeModel: Model<AccountTypeModel>,

        
    ) {}

    
    async get(): Promise<any[]> {
        return await this.AccountTypeModel.find().exec();
    }

    async getOne(_id: string): Promise<any[]> {
        return await this.AccountTypeModel.find({_id: _id, status:true});
    }

    async create(model: AccountTypeModel): Promise<any>{
        console.log(model);
        return await this.AccountTypeModel.create(model);
    }

    async update(_id: string, AccountTypeModel: AccountTypeModel): Promise<any> {
        return await this.AccountTypeModel.findByIdAndUpdate(_id, AccountTypeModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.AccountTypeModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}
