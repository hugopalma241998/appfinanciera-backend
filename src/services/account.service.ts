import { Model, Schema } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from 'src/commons/constants';
import { AccountModel } from 'src/models/Account.model';
import { Types } from 'mongoose';

@Injectable()
export class AccountService {

    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly acccountModel: Model<AccountModel>,
    ) { }




    async getAccountsByUser(_id: string, _idType): Promise<any[]> {
        const query = [
            {
                $match: {
                    userId: new Types.ObjectId(_id),
                    AccountTypeId: new Types.ObjectId(_idType),
                    status: true
                }
            },
            {
                $lookup:
                {
                    from: 'AccountTypes',
                    localField: 'AccountTypeId',
                    foreignField: '_id',
                    as: 'AccountType'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]
        

        return await this.acccountModel.aggregate(query);;

    }
    async getAccounts(): Promise<any[]> {
        const query = [

            {
                $lookup:
                {
                    from: 'accounttypes',
                    localField: 'AccountTypeId',
                    foreignField: '_id',
                    as: 'accounttypes'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]

        return await this.acccountModel.aggregate(query);
    }
    async getAcdcount(): Promise<any[]> {
        return await this.acccountModel.find();
    }
    async getAccount(_id: string): Promise<any[]> {
        return await this.acccountModel.find({ _id: _id, status: true });
    }

    async create(AccountModel: AccountModel): Promise<any> {
        //console.log(userModel);
        return await this.acccountModel.create(AccountModel);
    }

    async update(_id: string, AccountModel: AccountModel): Promise<any> {
        return await this.acccountModel.findByIdAndUpdate(_id, AccountModel, { new: true });
    }

    async delete(_id: string): Promise<any> {
        return await this.acccountModel.findByIdAndUpdate(_id, { status: false }, { new: true });
    }

}
