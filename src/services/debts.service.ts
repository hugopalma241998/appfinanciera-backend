import { Injectable, Inject } from '@nestjs/common';
import { USER_PROVIDER_KEY } from "../commons/constants";

import { Model, Types } from 'mongoose';
import { DebtTypeModel } from 'src/models/debtsType.model';
import { DebtsModel } from 'src/models/debts.model';



@Injectable()
export class DebtsService {
    
    constructor(
        @Inject(USER_PROVIDER_KEY)
        private readonly cardTypeModel: Model<DebtsModel>,

        
    ) {}


    async getAccountsByUser(_id: string): Promise<any[]> {
        const query = [
            {
                $match: {
                    userId: new Types.ObjectId(_id),
                    status: true
                }
            },  {
                $lookup:
                {
                    from: 'debtstypes',
                    localField: 'debtsTypeId',
                    foreignField: '_id',
                    as: 'debtstypes'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]
        

        return await this.cardTypeModel.aggregate(query);;

    }

    
    async get(): Promise<any[]> {
        const query = [

            {
                $lookup:
                {
                    from: 'debtstypes',
                    localField: 'debtsTypeId',
                    foreignField: '_id',
                    as: 'debtstypes'
                },


            },
            {
                $lookup:
                {
                    from: 'banks',
                    localField: 'bankId',
                    foreignField: '_id',
                    as: 'banks'
                },


            },
            {
                $lookup:
                {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                },


            }
        ]
        return await this.cardTypeModel.aggregate(query);
    }

    async getOne(_id: string): Promise<any[]> {
        return await this.cardTypeModel.find({_id: _id, status:true});
    }

    async create(model: DebtsModel): Promise<any>{
        console.log(model);
        return await this.cardTypeModel.create(model);
    }

    async update(_id: string, cardTypeModel: DebtsModel): Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, cardTypeModel, {new: true});
    }

    async delete(_id: string) : Promise<any> {
        return await this.cardTypeModel.findByIdAndUpdate(_id, {status: false}, {new: true});
    }

}