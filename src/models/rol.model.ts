
import { Document } from 'mongoose';

export interface RolModel extends Document {
    _id:string;
    name: string;
    description:string;
}