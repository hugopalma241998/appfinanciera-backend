
import { Document } from 'mongoose';


export interface DebtTypeModel extends Document {
    _id:string;
    name: string;
    description:string;
    status:boolean;
}
