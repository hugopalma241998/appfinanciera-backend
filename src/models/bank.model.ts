
import { Document, Schema } from 'mongoose';

export interface BankModel extends Document {
    _id:string;
    name: string;
    description: string;
    status: boolean;

}