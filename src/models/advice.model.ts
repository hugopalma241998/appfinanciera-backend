
import { Document } from 'mongoose';


export interface AdviceModel extends Document {
    _id:string;
    name: string;
    description:string;
    url:string;

    status:boolean;
}
