import { Document } from 'mongoose';

export interface CardTypeModel extends Document {
    _id:string;
    name: string;
    description:string;
    status:boolean;
}