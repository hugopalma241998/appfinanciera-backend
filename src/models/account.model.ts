import { Document, Schema } from 'mongoose';

export interface AccountModel extends Document {
    _id:string;
    name: string;
    description: string;
    status: boolean;
    amountAvailable: number;
    AccountTypeId:  { 
        type: Schema.Types.ObjectId;
        ref: 'AccountTypes';
    };
    bankId: {
        type: Schema.Types.ObjectId;
        ref: 'banks';
    };
    userId: {
        type: Schema.Types.ObjectId;
        ref: 'users';
    };
}
