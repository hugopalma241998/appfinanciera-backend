
import { Document, Schema } from 'mongoose';

export interface UserModel extends Document {
    _id:string;
    name: string;
    email: string;
    password: string;
    username: string;
    phone: number;
    status: boolean;
    roleId: {
        type: Schema.Types.ObjectId;
        ref: 'roles';
    },
    firstLogin: boolean;
}