import { Document, Schema } from 'mongoose';

export interface DebtsModel extends Document {
    _id:string;
    name: string;
    description: string;
    status: boolean;
    amountAvailable: number;
    fee: number;

    DebtsTypeId:  { 
        type: Schema.Types.ObjectId;
        ref: 'debtstypes';
    };
    bankId: {
        type: Schema.Types.ObjectId;
        ref: 'banks';
    };
    userId: {
        type: Schema.Types.ObjectId;
        ref: 'users';
    };
}
