import { Document, Schema } from 'mongoose';

export interface GoalModel extends Document {
    name: string;
    description: string;
    status: boolean;
    income: number;
    feeSaving: number;
    bill: number;
    userId: {
        type: Schema.Types.ObjectId;
        ref: 'users';
    };
}
