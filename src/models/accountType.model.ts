
import { Document } from 'mongoose';


export interface AccountTypeModel extends Document {
    _id:string;
    name: string;
    description:string;
    status:boolean;
}
