
import { Document, Schema } from 'mongoose';

export interface CardModel extends Document {
    _id:string;
    name: string;
    description: string;
    limit: number;
    dateOfPayment:Date;
    dateOfCut:Date;
    status: boolean;
    amountAvailable: number;
    cardTypeId:  { 
        type: Schema.Types.ObjectId;
        ref: 'cardtypes';
    };
    bankId: {
        type: Schema.Types.ObjectId;
        ref: 'banks';
    };
    userId: {
        type: Schema.Types.ObjectId;
        ref: 'users';
    };
}